/**
 * Created by Blindsight3D on 11/19/2017.
 */

var sql = require('mssql');
var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
    service: 'gmail',
    secure: false,
    port: 25,
    auth: {
        user: 'chiefpharmacist01@gmail.com',
        pass: 'adminuser$'
    },
    tls: {
        rejectUnauthorized: false
    }
});

//GET the list OF ITEMS
module.exports.getAllOrders = (config, callback) => {

    sql.connect(config, function(err) {
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
        // query to the database and get the records
        request.query('select * from order', function(err, recordset) {
            if (err) console.log(err)
                // send records as a response
            sql.close();
            callback(recordset.recordset);
        });
    });
}

//GET ORDERS BY ID
module.exports.getOrderById = (config, _id, callback) => {

    sql.connect(config, function(err) {
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
        // query to the database and get the records
        request.query("select * from order where orderId = " + _id, function(err, recordset) {
            if (err) console.log(err)
                // send records as a response
            sql.close();
            callback(recordset.recordset);
        });
    });
}

//PLACE ORDERS
module.exports.createOrder = (config, orders, callback) => {
    var lots = orders.lots;
    //console.log(lots);
    var insertQuery = '';
    var orderInsertQuery = '';
    var lotLastId = 1;
    var lotIds = [];
    var orderLastId = 1;
    sql.connect(config, function(err) {
        if (err) console.log(err);
        var request = new sql.Request();

        request.query("SELECT TOP 1 lotId FROM lot ORDER BY lotId DESC", function(err, recordset) {
            if (err) console.log(err)


            if (recordset.recordset[0] == null)
                lotLastId = 0;
            else
                lotLastId = recordset.recordset["0"].lotId;
            console.log(lotLastId);
            for (i = 0; i < lots.length; i++) {
                var x = lots[i];
                lotLastId++;
                lotIds.push(lotLastId);

                for (r = 0; r < x.length; r++) {
                    insertQuery += "(" + lotLastId + "," + x[r].itemId + ",'" + x[r].itemQty + "'," + x[r].totalItemPrice + "),";
                }
            }
            insertQuery = insertQuery.replace(/,\s*$/, "");
            insertQuery = "INSERT INTO lot(lotId,itemId,qty,totalItemPrice) VALUES" + insertQuery;

            request.query(insertQuery, function(err, recordset) {
                if (err) {
                    console.log(err);
                    //sql.close();
                } else {
                    request.query("SELECT TOP 1 orderId FROM orders ORDER BY orderId DESC", function(err, recordset) {
                        if (err) console.log(err);

                        if (recordset.recordset[0] == null)
                            orderLastId = 0;
                        else
                            orderLastId = recordset.recordset[0].orderId;
                        orderLastId++;
                        console.log(orderLastId);
                        for (i = 0; i < lotIds.length; i++) {
                            orderInsertQuery += "(" + orderLastId + "," + lotIds[i] + "," + orders.status + "," + orders.defaultSup + "," + orders.diffSup + "," + orders.isDiffSup + "," + orders.defaultSupStatus + "," + orders.diffSupStatus + ",'" + orders.createdDate + "','" + orders.ETA + "'," + orders.isDeleted + "," + orders.isDelivered + "," + orders.userId + "," + orders.orderPrice + ",'"+orders.supplierEmail+"'),";
                        }
                        console.log(orderInsertQuery);
                        orderInsertQuery = orderInsertQuery.replace(/,\s*$/, "");
                        orderInsertQuery = "INSERT INTO orders(orderId,lotId,status,defaultSup,diffSup,isDiffSup,defaultSupStatus,diffSupStatus,createdDate,ETA,isDeleted,isDelivered,userId,orderPrice,supplierEmail) VALUES" + orderInsertQuery;
                        console.log(orderInsertQuery);
                        request.query(orderInsertQuery, function(err, recordset) {
                            if (err) {
                                console.log(err);
                                sql.close();
                            }
                            callback("Data added");
                            sql.close();
                        });
                        //sql.close();
                    });
                    console.log("Data added");

                }
            });
            console.log(lotIds);
        });
    });
}

module.exports.getPendingOrdersForSup = (config, id, callback) => {

    sql.connect(config, function(err) {
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
        // query to the database and get the records
        request.query("SELECT DISTINCT(l.lotNo),o.*,l.lotNo,l.lotPrice,i.* FROM lot l, orders o,item i WHERE o.lotId=l.lotId AND l.itemId=i.itemId AND status=1 AND (defaultSup=" + id + " OR  diffSup=" + id + ") AND isDeleted=0", function(err, recordset) {
            if (err) console.log(err)
                // send records as a response
            sql.close();
            var result = recordset.recordset;
            callback(recordset.recordset);
        });
    });
}

module.exports.getOrdersForUser = (config, id, callback) => {

    sql.connect(config, function(err) {
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
        // query to the database and get the records
        request.query('SELECT DISTINCT(l.lotNo),o.*,l.lotNo,i.* FROM lot l, orders o,item i  WHERE o.lotId=l.lotId AND l.itemId=i.itemId AND isDeleted=0 AND  userId=' + id, function(err, recordset) {
            if (err) console.log(err)
            sql.close();
            callback(recordset.recordset);
        });
    });
}

module.exports.approveRejectOrders = (config, id, body, callback) => {

    sql.connect(config, function(err) {
        if (err) console.log(err);

        var request = new sql.Request();
        var upQuery = "UPDATE orders SET status=" + body.status + " WHERE lotId=" + id;
        console.log(upQuery)
        request.query(upQuery, function(err, recordset) {
            if (err) console.log(err)

            else {
                request.query("IF (Exists( SELECT * FROM orders where orderId=" + body.orderNo + " AND status=1 AND status!=4 AND status!=3)) SELECT 0 AS RESULT ELSE SELECT 1 AS RESULT", function(err, recordset) {
                    if (err) console.log(err)
                    else {
                        console.log(recordset.recordset[0].RESULT);
                        if (recordset.recordset[0].RESULT == 1) {
                            request.query("UPDATE orders SET status=4 WHERE status!=5 AND orderId=" + body.orderNo, function(err, recordset) {
                                if (err) console.log(err)
                                else {
                                    request.query("SELECT * FROM orders o, lot l, item i where o.lotId=l.lotId AND l.itemId=i.itemId AND status=4 AND orderId=" + body.orderNo, function(err, recordset) {
                                        if (err) console.log(err)
                                        else {
                                            var order = recordset.recordset;
                                            console.log(order)

                                             var mailBody="Hello Sir/Madam \n We need the following item to our company";
                                             for(i=0; i<order.length; i++)
                                             {
                                                 mailBody+="\n"+order[i].itemNo+"\t"+order[i].itemName+"\t"+order[i].qty;
                                             }
                                             console.log(mailBody);
                                            let mailOptions = {
                                                from: 'chiefpharmacist01@gmail.com', // sender address
                                                to: 'hardstudio.rambukwella@gmail.com', // list of receivers
                                                subject: 'Order Request', // Subject line
                                                text: mailBody, // plain text body
                                            };

                                            transporter.sendMail(mailOptions, (error, info) => {
                                                if (error) {
                                                    return console.log(error);
                                                }
                                                console.log('Message sent:');
                                                console.log('Preview URL:');
                                                callback("Mail sent");
                                                
                                            });
                                        }
                                        sql.close();
                                    });
                                }


                            });
                        } else {
                            sql.close();
                            callback("Data added");
                        }
                    }
                });

            }
        });
    });
}

module.exports.approveRejectOrdersDiffSup = (config, id, body, callback) => {
    sql.connect(config, function(err) {
        if (err) console.log(err);

        var request = new sql.Request();
        var upQuery = "UPDATE orders SET diffSupStatus=" + body.status + " WHERE lotId=" + id;
        console.log(upQuery)
        request.query(upQuery, function(err, recordset) {
            if (err) console.log(err)

               sql.close
               callback("Approved")
        });
    });

    }