var myApp= angular.module('myApp');
myApp.controller('goodsReceivingController',['$scope', '$http', '$location', '$routeParams','sharedProperties',function($scope, $http, $location, $routeParams,sharedProperties) {
	console.log('goodsReceivingController');
	//$scope.baseURL = sharedProperties.getURL();

	
	$scope.searchOrder = function (key) {
		document.getElementById("orderId").disabled = true;
		document.getElementById("supplierId").disabled = true;
		document.getElementById("amount").disabled = true;
		document.getElementById("deliveryId").disabled = true;
		document.getElementById("paymentType").disabled = true;
		document.getElementById("btnSendPayment").disabled = true;

		if((key=="")||(key==null)){
			alert("provide a valid orderId");
		}else{

			$http.get('http://localhost:3000/deusci/api/orderPayment/'+key).then(successCallback, errorCallback);
			function successCallback(response){
				   var orderDetails=response.data;
	
				   console.log(orderDetails);

				   if(orderDetails==[]){
					   alert("Order details not found");
					   $scope.orderId="";
					   $scope.receivedPayment="";
					   $scope.itemName="";
					   $scope.unitPrice="";
					   $scope.quantity="";
				   }else{
					$scope.orderId=orderDetails.orderId;
					$scope.receivedPayment=orderDetails.receivedPayment;
					$scope.itemName=orderDetails.itemName;
					$scope.unitPrice=orderDetails.unitPrice;
					$scope.quantity=orderDetails.qty;
				   }
	
				   
			}
			function errorCallback(error){
			}
	
		}


		
	};
	$scope.varifyPayment = function (receivedPayment,unitPrice,quantity) {
		if(receivedPayment<(unitPrice*quantity)){
			alert("Ordered amount not delivered");
			document.getElementById("orderId").disabled = true;
			document.getElementById("supplierId").disabled = true;
			document.getElementById("amount").disabled = true;
			document.getElementById("deliveryId").disabled = true;
			document.getElementById("paymentType").disabled = true;
			document.getElementById("btnSendPayment").disabled = true;
		}else{
			alert("Payment amount received, proceed with payment");
			document.getElementById("orderId").disabled = false;
			document.getElementById("supplierId").disabled = false;
			document.getElementById("amount").disabled = false;
			document.getElementById("deliveryId").disabled = false;
			document.getElementById("paymentType").disabled = false;
			document.getElementById("btnSendPayment").disabled = false;
		}
	};
	$scope.sendPayment=function(orderId,supplierId,amount,deliveryId,paymentType){

		if(((orderId==null)||(orderId==""))||((supplierId==null)||(supplierId==""))||((amount==null)||(amount==""))||((deliveryId==null)||(deliveryId==""))||((paymentType==null)||(paymentType==""))){
			alert("Provide Valid Values");
		}else{
			var paymentObj = {
				"orderId": orderId,
				"amount": amount,
				"paymentType": paymentType
			};
			var deliveryObj = {
				"orderId":orderId
			};

			
            
			$http.post('http://localhost:3000/deusci/api/payment', paymentObj).then(successCallback, errorCallback);
			function successCallback(response){
				console.log("payment Added Sucessfully");

				$http.post('http://localhost:3000/deusci/api/delivery', deliveryObj).then(successCallback, errorCallback);
				function successCallback(response){
					console.log("delivery Added Sucessfully");
					alert("Payment Sent");
					document.getElementById("orderId").disabled = true;
					document.getElementById("supplierId").disabled = true;
					document.getElementById("amount").disabled = true;
					document.getElementById("deliveryId").disabled = true;
					document.getElementById("paymentType").disabled = true;
					document.getElementById("btnSendPayment").disabled = true;

					document.getElementById("btnGenerateInvoice").disabled = false;
	
				}
				function errorCallback(error){
					console.log("error");
				}
			}
			function errorCallback(error){
				console.log("error");
			}
			
			

			console.log(paymentObj);
			console.log(deliveryObj);


		}

	};

	$scope.generateInvoice=function(orderId,supplierId,amount,deliveryId,paymentType){
		var pdf = new jsPDF();

		pdf.setFontSize(30);
		pdf.text(85, 30, 'Invoice');
		
		pdf.setFontSize(15);
		pdf.text(70, 40, 'Order Number     :'+orderId);
		pdf.text(70, 45, 'Supplier Number :'+supplierId);
		pdf.text(70, 50, 'Amount                :'+amount);
		pdf.text(70, 55, 'Delivery Number :'+deliveryId);
		pdf.text(70, 60, 'Payment Type    :'+paymentType);
        pdf.save('invoice.pdf');
	};

}]);