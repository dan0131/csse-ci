/**
 * Created by Blindsight3D on 11/22/2017.
 */
var myApp = angular.module('myApp');
myApp.controller('ApprovalOrderController', ['$scope', '$http', '$location', '$routeParams', '$rootScope', 'sharedProperties', function($scope, $http, $location, $routeParams, $rootScope, sharedProperties) {
    console.log('ApprovalOrderController');
    $scope.supID=localStorage['empId'];
    $scope.baseURL = sharedProperties.getURL();
    //GET ALL THE PENDING ITEMS
    $scope.getItems = function() {
        $http.get($scope.baseURL + '/deusci/api/orders/approvals/pending/'+$scope.supID).then(successCallback, errorCallback);

        function successCallback(response) {
            $scope.items = response.data;
            $scope.orders = response.data;
            $scope.orders = $scope.groupBy($scope.orders, "orderNo");
            $scope.modifiedOrders = $scope.getUniqLot($scope.orders);
            console.log($scope.orders);
            console.log($scope.modifiedOrders);
        }

        function errorCallback(error) {
            console.log(error);
        }
    }

    //GET the unique the LOT numbers
    $scope.getUniqLot = function(collection) {
        var res = [];
        for (i = 0; i < collection.length; i++) {
            res[i] = _.chain(collection[i]).map(function(item) {
                return item.lotNo[0]
            }).uniq().value();
        }
        for (i = 0; i < collection.length; i++) {
            res[i].defaultSup = collection[i].defaultSup
            res[i].diffSup = collection[i].diffSup
            res[i].diffSupStatus = collection[i].diffSupStatus
            res[i].isDiffSup = collection[i].isDiffSup
            res[i].orderNo = collection[i].orderNo
            res[i].orderPrice = collection[i].orderPrice
            res[i].userId=collection[i].userId
        }
        return res;

    }

    //GROUP the json Object
    $scope.groupBy = function(collection, property) {
        var i = 0,
            val, index,
            values = [],
            result = [];
        for (; i < collection.length; i++) {
            val = collection[i][property];
            val1 = collection[i]["isDiffSup"];
            val2 = collection[i]["diffSupStatus"];
            val3 = collection[i]["orderPrice"];
            val5 = collection[i]["defaultSup"];
            val4 = collection[i]["diffSup"];
            val6=collection[i]["userId"];
            index = values.indexOf(val);
            if (index > -1) {
                result[index].push(collection[i]);
                result[index].orderNo = val;
                result[index].isDiffSup = val1;
                result[index].diffSupStatus = val2;
                result[index].orderPrice = val3;
                result[index].diffSup = val4;
                result[index].defaultSup = val5;
                result[index].userId = val6;
            } else {
                values.push(val);
                result.push([collection[i]]);
            }
        }
        return result;
    }

    //DEFAULT Supervisor is APPROVAL
    $scope.approveReject = function(lotNo, status,orderNo) {
        
        
        lotNo = lotNo.replace(/\D/g,'');
        lotNo=parseInt(lotNo);
        orderNo = orderNo.replace(/\D/g,'');
        orderNo=parseInt(orderNo);
        reqObj={
            "status":status,
            "orderNo":orderNo
        }
        console.log(reqObj);
        $http.put($scope.baseURL + '/deusci/api/order/'+lotNo,reqObj).then(successCallback, errorCallback);
         function successCallback(response) {
            window.location.reload(false);
         }
         function errorCallback(error) {
            console.log(error);
        }

    }

    //DIFF Supervisor is APPROVAL
    $scope.approveRejectDiff = function(lotNo, status) {
        
        
        lotNo = lotNo.replace(/\D/g,'');
        lotNo=parseInt(lotNo);
        reqObj={
            "status":status
        }
        console.log(reqObj);
        $http.put($scope.baseURL + '/deusci/api/diff/order/'+lotNo,reqObj).then(successCallback, errorCallback);
         function successCallback(response) {
            window.location.reload(false);
         }
         function errorCallback(error) {
            console.log(error);
        }

    }

    //GET the list of items to given lot ID
    $scope.click1 = function(LotID) {
        
        $scope.items = [];

        LotID = LotID.replace(/\D/g,'');
        LotID=parseInt(LotID);

        for(var i = 0; i < $scope.orders.length ; i++) {

            //console.log($scope.orders[i]);

            for(var k = 0; k < $scope.orders[i].length ; k++){

               // console.log($scope.orders[i][k]);
                if($scope.orders[i][k].lotId == LotID){

                     $scope.items.push($scope.orders[i][k]);
                     console.log($scope.items);
                }



            }

        }

    }
}]);