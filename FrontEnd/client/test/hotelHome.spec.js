
describe('Testing Routes', function () {

// load the controller's module
beforeEach(angular.mock.module('myApp'));

it('should test routes',
inject(function ($route) {

  expect($route.routes['/hotelhome'].controller).toEqual('HotelHomeController');
  expect($route.routes['/hotelhome'].templateUrl).toEqual('views/hotelHome.html');

}));

});