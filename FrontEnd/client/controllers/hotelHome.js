var myApp= angular.module('myApp');
myApp.controller('HotelHomeController',['$scope', '$http', '$location', '$routeParams','$rootScope','sharedProperties',function($scope, $http, $location, $routeParams,$rootScope,sharedProperties) {
	console.log('HotelHomeController');
	$scope.baseURL = sharedProperties.getURL();
	$scope.errormsg="";
	$scope.oldHtl="";
	console.log($scope.errormsg="");
	console.log(localStorage['username']);

	$rootScope.$on("CallParentMethod", function(){
           $scope.getHotels();
        });

	$scope.getHotels = function(){
		$http.get($scope.baseURL+'deushrm/api/hotels').then(successCallback, errorCallback);
		function successCallback(response){
   			$scope.hotels=response.data;
   			console.log($scope.hotels);
		}
		function errorCallback(error){
			console.log(error);
		}
	}

	$scope.getHotel = function(){
		var id=$routeParams.id;
		$http.get($scope.baseURL+'deushrm/api/hotels/'+id).then(successCallback, errorCallback);
		function successCallback(response){
   			$scope.hotel=response.data;
   			$scope.htl=$scope.hotel[0];
   			$scope.oldHtl=$scope.htl;
   			$scope.htl.hotelRating=parseInt($scope.hotel[0].hotelRating);
   			console.log($scope.htl);
		}
		function errorCallback(error){
		}
	}

	$scope.deleteHotel= function(id){
		$http.delete($scope.baseURL+'deushrm/api/hotels/'+id).then(successCallback, errorCallback);
		function successCallback(response){
   			window.location.href="#/hotelhome";
		}
		function errorCallback(error){
		}
	}

	$scope.addNewHotel = function (htlName,htlRating,htlLocation,htlDescription,htlLongDescription,htlHomeImage,htlImageOutSide,htlImageInSide,htlImagePool,isWifi,isGym,isBar,isResturant,isSpa) {
		console.log(htlName+'/'+htlRating+'/'+htlLocation);
		if(htlName ==null || htlName=="")
		{
			$scope.errormsg="Hotel name is required";
		}
		else if(htlRating ==null || htlRating=="" )
		{
			$scope.errormsg="Hotel rating required";
		}
		else if(htlLocation== null || htlLocation =="")
		{
			$scope.errormsg="Hotel location required";
		}
		else
		{
				$scope.errormsg="";
   				var htlObj = {
   					"hotelName":htlName,
   					"hotelRating":htlRating+"",
   					"hotelLocation":htlLocation,
   					"hotelDescription":htlDescription,
   					"hotelLongDescription":htlLongDescription,
   					"hotelImageHome":htlHomeImage,
   					"hotelImageOutSide":htlImageOutSide,
   					"hotelImagePool":htlImageInSide,
   					"hotelImageInside":htlImagePool,
   					"isWifi":isWifi,
   					"isGym":isGym,
   					"isBar":isBar,
   					"isResturent":isResturant,
   					"isSpa":isSpa
   				};
   				console.log(htlObj);
   				$scope.errormsg="We are Processing Your Request !";
   				$http.post($scope.baseURL+'deushrm/api/hotels', htlObj).then(successCallback, errorCallback);
				function successCallback(response){
						$scope.errormsg="";
   						window.location.href="#/hotelhome";
				}
				function errorCallback(error){
    				console.log("errrrrrrrrrrrrrrrrrrrrrr");
				}
		}
	}

	$scope.editHotel = function (htlId,htlName,htlRating,htlLocation,htlDescription,htlLongDescription,htlImageHome,htlImageOutSide,hotelImageInside,hotelImagePool,isWifi,isGym,isBar,isResturent,isSpa) {
		console.log(htlName+'/'+htlRating+'/'+htlLocation);
		if(htlName ==null || htlName=="")
		{
			$scope.errormsg="Hotel name is required";
		}
		else if(htlRating ==null || htlRating=="" )
		{
			$scope.errormsg="Hotel rating is required";
		}
		else if(htlLocation== null || htlLocation =="")
		{
			$scope.errormsg="Hotel location is required";
		}
		else
		{
			$scope.errormsg="";
   				var htlObj = {
   					"hotelId":htlId,
   					"hotelName":htlName,
   					"hotelRating":htlRating+"",
   					"hotelLocation":htlLocation,
   					"hotelDescription":htlDescription,
   					"hotelLongDescription":htlLongDescription,
   					"hotelImageHome":htlImageHome,
   					"hotelImageOutSide":htlImageOutSide,
   					"hotelImageInside":hotelImageInside,
   					"hotelImagePool":hotelImagePool,
   					"isWifi":isWifi,
   					"isGym":isGym,
   					"isBar":isBar,
   					"isResturent":isResturent,
   					"isSpa":isSpa
   				};
   				console.log(htlObj);
   				if($scope.oldHtl===htlObj)
   				{
   					$scope.errormsg="There are no any changes !";
   				}
   				else
   				{
   					$scope.errormsg="We are Processing Your Request !";
   					$http.put($scope.baseURL+'deushrm/api/hotels/'+htlId, htlObj).then(successCallback, errorCallback);
					function successCallback(response){
						$scope.errormsg="";
   						window.location.href="#/hotelhome";
					}
					function errorCallback(error){
    					console.log("errrrrrrrrrrrrrrrrrrrrrr");
					}
				}
		}
	}

	$scope.range = function(min, max, step) {
    step = step || 1;
    var input = [];
    for (var i = min; i <= max; i += step) {
        input.push(i);
    }
    return input;
};
}]);