var sql = require('mssql');
/*module.exports.getAllHotels = (pool, callback) => {
	var query="select * from Hotels";
  	pool.request().query(query, (err, recordset) => {
    if(err)
    	callback(err);
    callback(recordset.recordset);

  });
}*/

module.exports.getAllHotels = (config, callback) => {

	sql.connect(config, function (err) {
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
        // query to the database and get the records
        request.query('select * from Hotels', function (err, recordset) {
            if (err) console.log(err)
            // send records as a response
        sql.close();
            callback(recordset.recordset);
        });
    });
}

module.exports.insertAHotel = (config,hotel,callback) => {
	var query="insert into Hotels(hotelName,hotelRating,hotelLocation,hotelDescription,hotelImageHome,hotelImageOutSide,hotelImagePool,hotelImageInside,isWifi,isGym,isBar,isResturent,isSpa,hotelLongDescription) values('"+hotel.hotelName+"', '"+hotel.hotelRating+"', '"+hotel.hotelLocation+"','"+hotel.hotelDescription+"', '"+hotel.hotelImageHome+"', '"+hotel.hotelImageOutSide+"', '"+hotel.hotelImagePool+"', '"+hotel.hotelImageInside+"', '"+hotel.isWifi+"', '"+hotel.isGym+"', '"+hotel.isBar+"', '"+hotel.isResturent+"', '"+hotel.isSpa+"', '"+hotel.hotelLongDescription+"')";
	console.log(query);
    sql.connect(config, function (err) {
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
        // query to the database and get the records
        request.query(query, function (err, recordset) {
            if (err) console.log(err)
            // send records as a response
        sql.close();
            callback('Data added successfully');
        });
    });
}

module.exports.getAHotel = (config,_id, callback) => {

	sql.connect(config, function (err) {
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
        // query to the database and get the records
        request.query("select * from Hotels where hotelId='"+_id+"'", function (err, recordset) {
            if (err) console.log(err)
            // send records as a response
        sql.close();
            callback(recordset.recordset);
        });
    });
}

module.exports.getAHotelByName = (config,_id, callback) => {

	sql.connect(config, function (err) {
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
        // query to the database and get the records
        request.query("select * from Hotels where hotelName LIKE '%"+_id+"%'", function (err, recordset) {
            if (err) console.log(err)
            // send records as a response
        sql.close();
            callback(recordset.recordset);
        });
    });
}

module.exports.deleteAHotel = (config,_id, callback) => {

	sql.connect(config, function (err) {
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
        // query to the database and get the records
        request.query("DELETE from RoomsReservation where hotelId='"+_id+"'", function (err, recordset) {
            if (err) console.log(err)
            // send records as a response
        });
        request.query("DELETE from Hotels where hotelId='"+_id+"'", function (err, recordset) {
            if (err) console.log(err)
            // send records as a response
        sql.close();
            callback("Deleted successfully!");
        });
    });
}

module.exports.updateAHotel = (config,hotel,_id,callback) => {
	var query="UPDATE Hotels SET hotelName = '"+hotel.hotelName+"', hotelRating = '"+hotel.hotelRating+"', hotelLocation='"+hotel.hotelLocation+"', hotelDescription='"+hotel.hotelDescription+"' , hotelImageHome='"+hotel.hotelImageHome+"', hotelImageOutSide='"+hotel.hotelImageOutSide+"', hotelImagePool='"+hotel.hotelImagePool+"', hotelImageInside='"+hotel.hotelImageInside+"', isWifi='"+hotel.isWifi+"', isGym='"+hotel.isGym+"', isBar='"+hotel.isBar+"', isResturent='"+hotel.isResturent+"', isSpa='"+hotel.isSpa+"', hotelLongDescription='"+hotel.hotelLongDescription+"' WHERE hotelId='"+_id+"'";
    console.log(query);
    sql.connect(config, function (err) {
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
        // query to the database and get the records
        request.query(query, function (err, recordset) {
            if (err) console.log(err)
            // send records as a response
        sql.close();
            callback('Data Updated successfully');
        });
    });
}
