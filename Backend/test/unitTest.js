var expect  = require('chai').expect;
var request = require('request');
let chai = require('chai');
var should = chai.should();


describe('Hotels', function() {
    describe ('Get all list of hotels', function(done) {
        it('Status', function(done) {
    		request('http://csseapi.azurewebsites.net/deushrm/api/hotels' , function(error, response, body) {
        	expect(response.statusCode).to.equal(200);
        	done();
    		});
		});

        it('content', function(done) {
            request('http://csseapi.azurewebsites.net/deushrm/api/hotels' , function(error, response, body) {
                expect(body).to.not.equal(null);
                done();
            });
        });
    });

    describe ('Get a hotel', function(done) {
        it('Status', function(done) {
    		request('http://csseapi.azurewebsites.net/deushrm/api/hotels/1' , function(error, response, body) {
        	expect(response.statusCode).to.equal(200);
        	done();
    		});
		});

        it('content', function(done) {
            request('http://csseapi.azurewebsites.net/deushrm/api/hotels/1' , function(error, response, body) {
                expect(body).to.not.equal(null);
                done();
            });
        });
    });
});

describe('Rooms', function() {
    describe ('Get Rooms', function(done) {
        it('Status', function(done) {
    		request('http://csseapi.azurewebsites.net/deushrm/api/roomstypes/rooms/6' , function(error, response, body) {
        	expect(response.statusCode).to.equal(200);
        	done();
    		});
		});

        it('content', function(done) {
            request('http://csseapi.azurewebsites.net/deushrm/api/roomstypes/rooms/6' , function(error, response, body) {
                expect(body).to.not.equal(null);
                done();
            });
        }); 
    });
});

describe('Room Types', function() {
    describe ('Get a Room Type', function(done) {
        it('Status', function(done) {
    		request('http://csseapi.azurewebsites.net/deushrm/api/roomtypes/hotels/1' , function(error, response, body) {
        	expect(response.statusCode).to.equal(200);
        	done();
    		});
		});

        it('content', function(done) {
            request('http://csseapi.azurewebsites.net/deushrm/api/roomtypes/hotels/1' , function(error, response, body) {
                expect(body).to.not.equal(null);
                done();
            });
        }); 
    });
    describe ('Get all Room Types in a Hotel', function(done) {
        it('Status', function(done) {
            request('http://csseapi.azurewebsites.net/deushrm/api/roomtypes/6' , function(error, response, body) {
            expect(response.statusCode).to.equal(200);
            done();
            });
        });

        it('content', function(done) {
            request('http://csseapi.azurewebsites.net/deushrm/api/roomtypes/6' , function(error, response, body) {
                expect(body).to.not.equal(null);
                done();
            });
        }); 
    });
});

describe('Messages', function() {
    describe ('Get Messages', function(done) {
        it('Status', function(done) {
            request('http://csseapi.azurewebsites.net/deushrm/api/messages' , function(error, response, body) {
            expect(response.statusCode).to.equal(200);
            done();
            });
        });

        it('content', function(done) {
            request('http://csseapi.azurewebsites.net/deushrm/api/messages' , function(error, response, body) {
                expect(body).to.not.equal(null);
                done();
            });
        }); 
    });
});
