var express = require('express');
var app = express();
var sql = require("mssql");
var bodyParser = require('body-parser');
var Hotels = require('./controllers/hotel.js');
var Employee = require('./controllers/employee.js');
var Order = require('./controllers/order.js');
var Item = require('./controllers/item.js');
var Lot = require('./controllers/lot.js');
var dbConfig = require('./config/dbConfig.js');
var orderPayment = require('./controllers/orderPayment.js');

app.use(bodyParser.json());

var config = dbConfig.dbConfig;



app.options("/*", function (req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
	res.send(200);
});
app.listen(process.env.PORT || 3000, (err) => {
	if (err)
		console.log(err);
	else
		console.log('Connected');
});


//Employee APIs

app.post('/deusci/api/employees', function (req, res) {
	var emp = req.body;
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');	
	Employee.createUser(config, emp, recs => {
		res.json(recs);
	});
});

app.get('/deusci/api/employee/:id', function (req, res) {
	res.header("Access-Control-Allow-Origin", "*");
	Employee.getEmployeeById(config, req.params.id, recs => {
		if (recs == null)
			res.status(400).json({ message: "NotFound" });

		else
			res.status(200).json(recs);
	});
});

app.get('/deusci/api/employees/managers', function (req, res) {
	res.header("Access-Control-Allow-Origin", "*");
	Employee.getManagers(config, recs => {
		if (recs == null)
			res.status(400).json({ message: "NotFound" });

		else
			res.status(200).json(recs);
	});
});

app.get('/deusci/api/suplliers', function (req, res) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');	
	Employee.getSuppliers(config, recs => {
		res.json(recs);
	});
});
//Order APIs

app.get('/deusci/api/order/:id', function (req, res) {
	res.header("Access-Control-Allow-Origin", "*");
	Order.getOrderById(config, req.params.id, recs => {
		res.json(recs);
	});
});

app.get('/deusci/api/orders', function (req, res) {
	res.header("Access-Control-Allow-Origin", "*");
	Order.getAllOrders(config, recs => {
		res.json(recs);
	});
});

app.post('/deusci/api/order', function (req, res) {
	var order = req.body;
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
	//console.log(req.body);
	Order.createOrder(config, order, recs => {
		res.json(recs);
	});
});

app.put('/deusci/api/order/:_id', function (req, res) {
	var order = req.body;
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
	//console.log(req.body);
	Order.approveRejectOrders(config,req.params._id, order, recs => {
		res.json(recs);
	});
});

app.put('/deusci/api/diff/order/:_id', function (req, res) {
	var order = req.body;
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
	//console.log(req.body);
	Order.approveRejectOrdersDiffSup(config,req.params._id, order, recs => {
		res.json(recs);
	});
});

app.get('/deusci/api/orders/approvals/pending/:_id', function (req, res) {
	res.header("Access-Control-Allow-Origin", "*");
	Order.getPendingOrdersForSup(config, req.params._id, recs => {
		if (recs == null)
			res.status(400).json({ message: "NotFound" });
		else
			res.status(200).json(recs);
	});
});

app.get('/deusci/api/orders/:_id', function (req, res) {
	res.header("Access-Control-Allow-Origin", "*");
	Order.getOrdersForUser(config, req.params._id, recs => {
		if (recs == null)
			res.status(400).json({ message: "NotFound" });
		else
			res.status(200).json(recs);
	});
});

//Item APIs

app.get('/deusci/api/items', function (req, res) {
	res.header("Access-Control-Allow-Origin", "*");
	Item.getAllItems(config, recs => {
		res.json(recs);
	});
});

//Lot APIs

app.get('/deusci/api/lots', function (req, res) {
	res.header("Access-Control-Allow-Origin", "*");
	Lot.getAllLots(config, recs => {
		res.json(recs);
	});
});

app.get('/deusci/api/lot/:id', function (req, res) {
	res.header("Access-Control-Allow-Origin", "*");
	Lot.getLotById(config, req.params.id, recs => {
		res.json(recs);
	});
});

//orderPayment APIs
app.get('/deusci/api/orderPayment/:orderId', function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    orderPayment.getOrderPaymentById(config, req.params.orderId, recs => {
        res.json(recs);
});
});

app.post('/deusci/api/payment', function(req, res) {
	var payment = req.body;
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
	//console.log(req.body);
	orderPayment.addPayment(config, payment, recs => {
		res.json(recs);
	});
});

app.post('/deusci/api/delivery', function(req, res) {
	var delivery = req.body;
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
	//console.log(req.body);
	orderPayment.addDelivery(config, delivery, recs => {
		res.json(recs);
	});
});




