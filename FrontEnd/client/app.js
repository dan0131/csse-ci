var myApp = angular.module('myApp', ['ngRoute']);
myApp.config(function($routeProvider, $locationProvider) {
	//debugger;
	$locationProvider.hashPrefix('');
	$routeProvider.when('/home', {
			resolve: {
				"check": function($location) {
					console.log("--" + localStorage['email'])
					if (localStorage['email'] == null)
						$location.path('/404')
				}
			},
			controller: 'HomeController',
			templateUrl: 'views/home.html'
		})
		.when('/', {
			resolve: {
				"check": function($location) {
					console.log("--" + localStorage['email'])
					if (localStorage['email'] == null)
						$location.path('/404')
				}
			},
			redirectTo: '/auth'
		})
		.when('/auth', {
			controller: 'AuthController',
			templateUrl: 'views/auth.html'
		})
		.when('/reg', {
			controller: 'RegController',
			templateUrl: 'views/reg.html'
		})
	.when('/404', {
			resolve: {
				"check": function($location) {
					console.log("--" + localStorage['email'])
					if (localStorage['email'] == 'null' || localStorage['email'] == undefined)
						$location.path('/auth')
				}
			},
			templateUrl: 'views/404.html'
		})
		.otherwise({
			redirectTo: '/404'
		})

	.when('/order/add', {
		resolve: {
			"check": function($location) {
				console.log("--" + localStorage['email'])
				if (localStorage['email'] == null)
					$location.path('/404')
			}
		},
		controller: 'OrderController',
		templateUrl: 'views/createOrder.html'
	})

        .when('/order/view', {
            resolve: {
                "check": function($location) {
                    console.log("--" + localStorage['email'])
                    if (localStorage['email'] == null)
                        $location.path('/404')
                }
            },
            controller: 'AllOrderController',
            templateUrl: 'views/viewOrder.html'
        })
	.when('/order/approval', {
		resolve: {
			"check": function($location) {
				console.log("--" + localStorage['email'])
				if (localStorage['email'] == null)
					$location.path('/404')
			}
		},
		controller: 'ApprovalOrderController',
		templateUrl: 'views/approvalOrder.html'
	})
	
	// dev-Niru start
		.when('/orderPaymentHome', {
			resolve:{
				"check":function($location)
				{ console.log("--"+localStorage['username'])
					if(localStorage['username'] != 'admin')
					$location.path('/404')
				}
			},
			controller: 'paymentHomeController',
			templateUrl: 'views/paymentHome.html'
		})
		.when('/orderPaymentHome/goodsReceiving', {
			resolve:{
				"check":function($location)
				{ console.log("--"+localStorage['username'])
					if(localStorage['username'] != 'admin')
					$location.path('/404')
				}
			},
			controller: 'goodsReceivingController',
			templateUrl: 'views/goodsReceiving.html'
		})
		// dev-Niru end


})