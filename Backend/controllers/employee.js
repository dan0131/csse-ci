/**
 * Created by Blindsight3D on 11/19/2017.
 */

var sql = require('mssql');

module.exports.getEmployeeById = (config, _id, callback) => {

    sql.connect(config, function (err) {
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
        // query to the database and get the records
        request.query("select * from employee where email like '%" + _id + "%'", function (err, recordset) {
            if (err) console.log(err)
            // send records as a response
            sql.close();
            callback(recordset.recordset[0]);
        });
    });
}
module.exports.getManagers = (config, callback) => {

    sql.connect(config, function (err) {
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
        // query to the database and get the records
        request.query("select * from employee where role = " + 2, function (err, recordset) {
            if (err) console.log(err)
            // send records as a response
            sql.close();
            callback(recordset.recordset);
        });
    });
}

module.exports.getSuppliers = (config, callback) => {

    sql.connect(config, function (err) {
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
        // query to the database and get the records
        request.query("select * from suppliers", function (err, recordset) {
            if (err) console.log(err)
            // send records as a response
            sql.close();
            callback(recordset.recordset);
        });
    });
}

module.exports.createUser = (config, user, callback) => {

    var insertQuery = '';

    sql.connect(config, function (err) {
        if (err) console.log(err);
        var request = new sql.Request();
        insertQuery = insertQuery.replace(/,\s*$/, "");
        insertQuery = "('" + user.name + "','" + user.address + "','" + user.email + "','" + user.site + "','"+user.role + "','" + user.supervisorId + "','" + user.issupervisor + "','" + user.password+ "')";

        insertQuery = "INSERT INTO employee(name,address,email,site,role,supervisorId,issupervisor,password) VALUES" + insertQuery;

        request.query(insertQuery, function (err, recordset) {
            if (err) {
                console.log(err);
                sql.close();
            }
            else {
                console.log(user);
                console.log('Data added!');
                sql.close();
            }
        });
        console.log(user);
    });
}