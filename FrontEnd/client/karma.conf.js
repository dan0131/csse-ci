
module.exports = function(config) {
  config.set({

    
    basePath: '',


    
    frameworks: ['jasmine'],


    
    files: [
    'node_modules/angular/angular.js',
    'node_modules/angular-route/angular-route.js',
    'node_modules/angular-mocks/angular-mocks.js',
    'app.js',
    './controllers/hotelHome.js',
    './test/hotelHome.spec.js'
    ],


    
    exclude: [
    ],


    
    preprocessors: {
    },


    
    reporters: ['progress'],


    


    
    colors: true,


    
    logLevel: config.LOG_INFO,


    
    autoWatch: true,


    
    browsers: ['Chrome'],


    
    singleRun: false,

    
    concurrency: Infinity
  })
}
