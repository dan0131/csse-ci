/**
 * Created by Blindsight3D on 11/19/2017.
 */

var sql = require('mssql');

module.exports.getOrderPaymentById = (config,orderId, callback) => {

    sql.connect(config, function (err) {
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
        // query to the database and get the records
        request.query("select o.orderId, o.receivedPayment, i.itemName, i.unitPrice, l.qty from orderPayment o, lot l, item i where o.lotId=L.lotId and l.itemId=i.itemId and o.orderId="+orderId, function (err, recordset) {
            if (err) console.log(err)
            // send records as a response
            sql.close();
            callback(recordset.recordset[0]);
        });
    });
}

module.exports.addPayment = (config,payment,callback) => {
	var query="insert into payment values(getDate(), '"+payment.orderId+"', '"+payment.amount+"','"+payment.paymentType+"')";
	console.log(query);
    sql.connect(config, function (err) {
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
        // query to the database and get the records
        request.query(query, function (err, recordset) {
            if (err) console.log(err)
            // send records as a response
        sql.close();
            callback('Data added successfully');
        });
    });
}

module.exports.addDelivery = (config,delivery,callback) => {
	var query="insert into delivery values(getDate(), 'pending', '"+delivery.orderId+"')";
	console.log(query);
    sql.connect(config, function (err) {
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
        // query to the database and get the records
        request.query(query, function (err, recordset) {
            if (err) console.log(err)
            // send records as a response
        sql.close();
            callback('Data added successfully');
        });
    });
}