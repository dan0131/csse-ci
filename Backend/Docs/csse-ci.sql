	create TABLE role
(
	roleId int identity(1,1),
	roleName varchar(30),
	isActive int,

	constraint role_pk PRIMARY KEY (roleId)
)

create TABLE employee
(
	empId int identity(1,1),
	empNo varchar(10),
	name varchar(30),
	address varchar(40),
	email	varchar(60),
	site	varchar(30),
	role int,
	supervisorId int,
	issupervisor varchar(6) default 'false',

	constraint employee_pk PRIMARY KEY (empId),
	constraint employee_fk1 FOREIGN KEY (role) REFERENCES role,
	constraint employee_fk2 FOREIGN KEY (supervisorId)	REFERENCES employee,
)

create TABLE suppliers
(
	supID int identity(1,1),
	name varchar(30),
	email	varchar(60),

	constraint employee_pk PRIMARY KEY (supID),
)


create table item
(
	itemId int identity(1,1),
	itemNo varchar(15),
	itemName varchar(50),
	unitPrice float,
	unitOfMessurements varchar(50),
	type varchar(20),

	constraint item_pk PRIMARY KEY (itemId),
)

create table lot
(
	lotId int,
	lotNo varchar(20),
	itemId int,
	qty int,
 	created date,

	constraint lot_pk PRIMARY KEY (lotId,itemId),
	constraint lot_fk1 FOREIGN KEY (itemId) REFERENCES item

)

create table status
(
	statusId int identity(1,1),
	statusDescription varchar(50)

	constraint status_pk PRIMARY KEY (statusId)
)

create table orders
(
	orderId int,
	orderNo varchar(20),
	lotId int,
	status int,
	defaultSup int,
	diffSup int DEFAULT NULL,
	isDiffSup int,
	defaultSupStatus int,
	diffSupStatus int,
	createdDate date,
	ETA date,
	isDeleted int,
	isDelivered int,
	userId

	constraint order_pk PRIMARY KEY (orderId,lotId),
	constraint order_fk2 FOREIGN KEY (defaultSup) REFERENCES employee,
	constraint order_fk3 FOREIGN KEY (diffSup) REFERENCES employee,
	constraint order_fk4 FOREIGN KEY (diffSup) REFERENCES employee,
	constraint order_fk5 FOREIGN KEY (status) REFERENCES status,
	constraint order_fk6 FOREIGN KEY (defaultSupStatus) REFERENCES status,
	constraint order_fk7 FOREIGN KEY (diffSupStatus) REFERENCES status
)
SELECT * from item
INSERT INTO employee(name,address,email,site,role) VALUES('Hasthi','Kandy','hasthi.r@deus.com','Malabe',1)
INSERT INTO employee(name,address,email,site,role) VALUES('Argon','Kandy','argon.w@deus.com','Malabe',2)
INSERT INTO employee(name,address,email,site,role) VALUES('Danika','Kandy','danika.d@deus.com','Malabe',2)

INSERT INTO employee(name,address,email,site,role) VALUES('Nirushanth','Jaffna','niru.n@deus.com','Malabe',1)
SELECT SCOPE_IDENTITY() AS id

INSERT INTO item(itemName,unitPrice,unitOfMessurements,type) VALUES('Tiles',2000,'Boxes','Solid')
INSERT INTO item(itemName,unitPrice,unitOfMessurements,type) VALUES('Sand',3000,'CubicMeters','Solid')
INSERT INTO item(itemName,unitPrice,unitOfMessurements,type) VALUES('Wires',3000,'Rolls','Metal')
INSERT INTO item(itemName,unitPrice,unitOfMessurements,type) VALUES('Nails',1500,'Boxes','Metal')
INSERT INTO item(itemName,unitPrice,unitOfMessurements,type) VALUES('Paints',1500,'Buckets','Liquid')
INSERT INTO item(itemName,unitPrice,unitOfMessurements,type) VALUES('Cement',1000,'Packets','Power')
INSERT INTO item(itemName,unitPrice,unitOfMessurements,type) VALUES('Nails',1500,'Boxes','Metal')
SELECT * from lot
INSERT INTO lot(lotId,itemId,qty,created) VALUES(1,1,20,'2017-11-19')
INSERT INTO lot(lotId,itemId,qty,created) VALUES(1,2,10,'2017-11-19')
INSERT INTO lot(lotId,itemId,qty,created) VALUES(2,1,3,'2017-11-30')

SELECT * from status
SELECT * from role

SELECT * from orders
INSERT INTO orders(orderId,lotId,status,defaultSup,diffSup,isDiffSup,defaultSupStatus,diffSupStatus,createdDate,ETA,isDeleted,isDelivered) 
VALUES(1,2,1,1,NULL,0,1,NULL,'2017-11-29','2017-12-19',0,0)
INSERT INTO orders(orderId,lotId,status,defaultSup,diffSup,isDiffSup,defaultSupStatus,diffSupStatus,createdDate,ETA,isDeleted,isDelivered) 
VALUES(2,3,1,3,2,1,1,1,'2017-11-29','2017-12-19',0,0)

SELECT DISTINCT(l.lotNo),o.*,l.lotNo FROM lot l, orders o
WHERE o.lotId=l.lotId

SELECT TOP 1 lotId FROM lot ORDER BY lotId DESC
DROP TABLE status

INSERT INTO lot(lotId,itemId,qty) VALUES(3,1,'12'),(4,2,'20')

DELETE FROM item
WHERE lotId=6

select * from status

Update  item
SET maxQty=150
WHERE itemId=14

ALTER TABLE orders
ADD userId int CONSTRAINT fk_8 FOREIGN KEY (userId) REFERENCES employee

ALTER TABLE lot
ADD lotPrice int

ALTER TABLE orders
ADD orderPrice int

ALTER TABLE employee
ADD password varchar(60)

ALTER TABLE item
ADD maxQty int
ALTER TABLE lot
ADD totalLotPrice int

SELECT DISTINCT(l.lotNo),o.*,l.lotNo,l.lotPrice,i.* FROM lot l, orders o,item i WHERE o.lotId=l.lotId AND l.itemId=i.itemId AND status=1 AND (defaultSup=3 OR  diffSup=3) AND isDeleted=0

SELECT DISTINCT(l.lotNo),o.*,l.lotNo FROM lot l, orders o
WHERE o.lotId=l.lotId AND status=1 AND (defaultSup=3 OR  diffSup=3) AND isDeleted=0

IF ( Exists (SELECT * from orders WHERE isDiffSup = 1)) 
SELECT DISTINCT(l.lotNo),o.*,l.lotNo,i.* FROM lot l, orders o,item i  
WHERE o.lotId=l.lotId AND l.itemId=i.itemId AND status=1 AND (defaultSup=3 OR  diffSup=3) AND (isDiffSup= 1 AND diffSupStatus = 2) 
Else 
SELECT DISTINCT(l.lotNo),o.*,l.lotNo,i.* FROM lot l, orders o,item i  
WHERE o.lotId=l.lotId AND l.itemId=i.itemId AND status=1 AND (defaultSup=3 OR  diffSup=3)

DELETE FROM 

insert into status(statusDescription) VALUES('Rejected')
select * from orders

SELECT DISTINCT(l.lotNo),o.*,l.lotNo,i.* FROM lot l, orders o,item i  WHERE o.lotId=l.lotId AND l.itemId=i.itemId AND isDeleted=0 AND  userId=1

UPDATE orders SET status=1 WHERE lotId=38	


UPDATE orders SET diffSupStatus=2 WHERE lotId=43

IF (Exists( SELECT * FROM orders where orderId=1 AND status=1 AND status!=5 AND status!=4 AND status!=3)) SELECT 0 AS RESULT ELSE SELECT 1 AS RESULT