/**
 * Created by Blindsight3D on 11/19/2017.
 */

var sql = require('mssql');

module.exports.getAllLots  = (config, callback) => {

    sql.connect(config, function (err) {
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
        // query to the database and get the records
        request.query('select * from lot', function (err, recordset) {
            if (err) console.log(err)
            // send records as a response
            sql.close();
            callback(recordset.recordset);
        });
    });
}

module.exports.getLotById = (config,_id, callback) => {

    sql.connect(config, function (err) {
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
        // query to the database and get the records
        request.query("select * from lot where lotId = "+_id , function (err, recordset) {
            if (err) console.log(err)
            // send records as a response
            sql.close();
            callback(recordset.recordset);
        });
    });
}