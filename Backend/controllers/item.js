/**
 * Created by Blindsight3D on 11/19/2017.
 */

var sql = require('mssql');

module.exports.getAllItems  = (config, callback) => {

    sql.connect(config, function (err) {
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
        // query to the database and get the records
        request.query('select * from item', function (err, recordset) {
            if (err) console.log(err)
            // send records as a response
            sql.close();
            callback(recordset.recordset);
        });
    });
}