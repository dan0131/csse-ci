/**
 * Created by Blindsight3D on 11/22/2017.
 */
var myApp= angular.module('myApp');
myApp.controller('OrderController',['$scope', '$http', '$location', '$routeParams','$rootScope','sharedProperties',function($scope, $http, $location, $routeParams,$rootScope,sharedProperties) {
    console.log('OrderController');

    $scope.baseURL = sharedProperties.getURL();
    $scope.lot = [];
    $scope.parentLot = [];
    $scope.order = {};
    $scope.items;
    $scope.ETADATE;
    var finalItems;
    $scope.allItems;
    $scope.isShowSuccsessAlert = false;
    $scope.ErrorString = null;
    $scope.getItems = function () {
        $http.get($scope.baseURL + '/deusci/api/items').then(successCallback, errorCallback);
        function successCallback(response) {
            finalItems = response.data;
            $scope.allItems = response.data;
            $scope.items = finalItems;
            console.log($scope.items);
            $scope.getSupervisors();
        }

        function errorCallback(error) {
            console.log(error);
        }
    }

    $scope.getItem  = function(){
        var id=$routeParams.id;
        $http.get($scope.baseURL+'deushrm/api/hotels/'+id).then(successCallback, errorCallback);
        function successCallback(response){
            $scope.item=response.data;
            console.log($scope.item);
        }
        function errorCallback(error){
        }
    }
    
    $scope.getSupervisors = function () {
        $http.get($scope.baseURL+'/deusci/api/employees/managers/').then(successCallback, errorCallback);
        function successCallback(response){
            $scope.supervisors = response.data;
            console.log($scope.supervisors);
            $scope.getDefaultSup();
            $scope.getSuppliers();
        }
        function errorCallback(error){
        }
    }

    $scope.getSuppliers = function () {
        $http.get($scope.baseURL+'/deusci/api/suplliers/').then(successCallback, errorCallback);
        function successCallback(response){
            $scope.suppliers = response.data;
            console.log($scope.suppliers);
        }
        function errorCallback(error){
        }
    }

    $scope.getDefaultSup = function () {
        var id = localStorage["defualtSup"];
        for(var i = 0; i < $scope.supervisors.length ; i++) {
            if($scope.supervisors[i].empId == id){
                $scope.defaultname = $scope.supervisors[i].name;
            }
        }
    }

    $scope.addToLot = function (item) {
        console.log(item);
        if($scope.lot.length == 0) {
            item.itemQty = null;
            $scope.lot.push(item);
            $scope.index =  $scope.items.indexOf(item);
            $scope.items.splice($scope.index, 1);
        }
        else {

            if($scope.lot.indexOf(item)== -1) {
                console.log("push");
                item.itemQty = null;
                $scope.lot.push(item);
                $scope.index =  $scope.items.indexOf(item);
                $scope.items.splice($scope.index, 1);
            }
            else {
                console.log("exists");
            }
        }
        console.log($scope.lot);
    }

    $scope.addToOrder = function () {
        if($scope.lot.length != 0) {
            $scope.calculateTotalLotPrice();
            console.log($scope.lot);
            $scope.lot.lotId = $scope.parentLot.length + 1;
            $scope.parentLot.push($scope.lot);
            console.log($scope.parentLot);
            $scope.lot = [];
            console.log($scope.lot);
            $scope.calculateOrderTotalPrice();
        } else {
            $scope.isShowSuccsessAlert = true;
            $scope.ErrorString = "Lot cannot be empty";
            console.log("Lot Is empty");
        }
    }

    $scope.placeOrder = function () {

        if($scope.parentLot.length != 0) {

            $scope.order.lots = $scope.parentLot;

            $scope.createOrder();

            console.log($scope.order);

            $http.post($scope.baseURL + '/deusci/api/order', $scope.order).then(successCallback, errorCallback);
            function successCallback(response) {
                console.log("Post Successfull ");
                window.location.reload(false);
            }
            function errorCallback(error) {
                console.log(error);
            }
        } else {
            $scope.isShowSuccsessAlert = true;
            $scope.ErrorString = "Order cannot be empty";
            console.log("Parent lot is empty");
        }
    }

    $scope.clearLot = function () {
        for(var i = 0; i < $scope.lot.length ; i++) {
            $scope.items.push($scope.lot[i]);
        }
        $scope.lot=[];
        $scope.totalItemPrice = null;
    }

    $scope.clearOrder = function () {

        console.log("length of finalItems "+finalItems.length);

        if($scope.parentLot.length != 0){
            $scope.getItems();
            $scope.parentLot = [];
            $scope.totalOrderPrice = 0;
        }
    }

    $scope.calculateTotalItemPrice = function (l) {

        console.log("keypress ");
        console.log(l);
        l.totalItemPrice = l.unitPrice*l.itemQty;

    }

    $scope.calculateTotalLotPrice = function () {

        var totalLotPrice = 0;

        for(var i = 0; i < $scope.lot.length ; i++) {
            totalLotPrice = totalLotPrice + $scope.lot[i].totalItemPrice;
        }
        $scope.lot.totalLotPrice = totalLotPrice;
    }

    $scope.calculateOrderTotalPrice = function () {

        console.log("parent lot " +$scope.parentLot);

        var totalOrderPrice = 0;

        for(var i = 0; i < $scope.parentLot.length ; i++) {
            totalOrderPrice = totalOrderPrice + $scope.parentLot[i].totalLotPrice;
        }

        $scope.totalOrderPrice = totalOrderPrice;
    }

    $scope.isOverLimit = function () { // check weather the total price is over 100000

        if( $scope.totalOrderPrice > 100000){
            return true;
        } else {
            return false;
        }

    }

    $scope.createOrder = function () {

        $scope.date;
        var e = document.getElementById("supIdDropDown");
        var strUser = e.options[e.selectedIndex].value;

        var f = document.getElementById("SupplierIdDropDown");
        var supplierEmail = f.options[f.selectedIndex].value;

        date = new Date($('#ETA').val());
        $scope.date =  date.getFullYear()+"-"+(date.getMonth() + 1)+"-"+date.getDate();

        console.log($scope.date);

        if($scope.totalOrderPrice > 100000){ // order object when total price is over 100000
            if(strUser == localStorage["defualtSup"]){
                console.log("default selected")
                $scope.order.status = 1;
                $scope.order.defaultSup = localStorage["defualtSup"];
                $scope.order.diffSup = null;
                $scope.order.isDiffSup = 0;
                $scope.order.defaultSupStatus = 1;
                $scope.order.diffSupStatus = null;
                $scope.order.createdDate = $scope.getCurrentDate();
                $scope.order.ETA = $scope.date;
                $scope.order.isDeleted = 0;
                $scope.order.isDelivered = 0;
                $scope.order.userId = 1
                $scope.order.orderPrice = $scope.totalOrderPrice;
                $scope.order.supplierEmail = supplierEmail;
            } else {
                console.log("default NOT selected")
                $scope.order.status = 1;
                $scope.order.defaultSup = localStorage["defualtSup"];
                $scope.order.diffSup = strUser;
                $scope.order.isDiffSup = 1;
                $scope.order.defaultSupStatus = 1;
                $scope.order.diffSupStatus = 1;
                $scope.order.createdDate = $scope.getCurrentDate();
                $scope.order.ETA = $scope.date;
                $scope.order.isDeleted = 0;
                $scope.order.isDelivered = 0;
                $scope.order.userId = 1
                $scope.order.orderPrice = $scope.totalOrderPrice;
                $scope.order.supplierEmail = supplierEmail;
            }
        }else{ // order object when total price is less than 100000
            $scope.order.status = 4;
            $scope.order.defaultSup = null;
            $scope.order.diffSup = null;
            $scope.order.isDiffSup = null;
            $scope.order.defaultSupStatus = null;
            $scope.order.diffSupStatus = null;
            $scope.order.createdDate = $scope.getCurrentDate();
            $scope.order.ETA = $scope.date;
            $scope.order.isDeleted = 0;
            $scope.order.isDelivered = 0;
            $scope.order.userId = 1
            $scope.order.orderPrice = $scope.totalOrderPrice;
            $scope.order.supplierEmail = supplierEmail;
        }

    }

    $scope.getCurrentDate = function () {

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!

        var yyyy = today.getFullYear();
        if(dd<10){
            dd='0'+dd;
        }
        if(mm<10){
            mm='0'+mm;
        }

        var today = yyyy+'-'+mm+'-'+dd;

        return today;

       // document.getElementById('ETA').valueAsDate = new Date();

    }

    $scope.setCurrentDateToETA = function () {
        document.getElementById('ETA').valueAsDate = new Date();
    }

    $scope.setMinDate = function () { // This function sets the minimum date of the date picker. You cant select dates before minimum date

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        if(dd<10){
            dd='0'+dd
        }
        if(mm<10){
            mm='0'+mm
        }

        today = yyyy+'-'+mm+'-'+dd;
        document.getElementById("ETA").setAttribute("min", today);

    }

    $scope.showHide = function () { // this function hides the alter dialog when called

        $scope.isShowSuccsessAlert = false;

    }


}]);

