/**
 * Created by Blindsight3D on 11/25/2017.
 */
var myApp= angular.module('myApp');
myApp.controller('AllOrderController',['$scope', '$http', '$location', '$routeParams','$rootScope','sharedProperties',function($scope, $http, $location, $routeParams,$rootScope,sharedProperties) {
    console.log('AllOrderController');

    $scope.baseURL = sharedProperties.getURL();
    $scope.finalItems;
    $scope.items = [];
    $scope.getItems = function () {
        $http.get($scope.baseURL + '/deusci/api/orders/'+localStorage["empId"]).then(successCallback, errorCallback);
        function successCallback(response) {
            $scope.orders = response.data;
            $scope.orders=$scope.groupBy($scope.orders, "orderNo");
            $scope.modifiedOrders=$scope.getUniqLot($scope.orders);
           // $scope.getItemsByLotId("LOT002");
        }
        function errorCallback(error) {
            console.log(error);
        }
    }

    $scope.getItemsByLotId = function (LotID) {

        $scope.items = [];

        LotID = LotID.replace(/\D/g,'');
        LotID=parseInt(LotID);

        for(var i = 0; i < $scope.orders.length ; i++) {
            for(var k = 0; k < $scope.orders[i].length ; k++){
                if($scope.orders[i][k].lotId == LotID){
                     $scope.items.push($scope.orders[i][k]);
                     console.log($scope.items);
                }
            }
        }
        return $scope.items;
    }

    $scope.getUniqLot=function (collection) { // this function returns the details of unique Lots
        var res=[];
        for(i=0;i<collection.length;i++)
        {
            res[i]=_.chain(collection[i]).map(function(item) { return item.lotNo[0] }).uniq().value();
        }
        for(i=0;i<collection.length;i++)
        {
            res[i].defaultSup=collection[i].defaultSup
            res[i].diffSup=collection[i].diffSup
            res[i].diffSupStatus=collection[i].diffSupStatus
            res[i].isDiffSup=collection[i].isDiffSup
            res[i].orderNo=collection[i].orderNo
            res[i].orderPrice=collection[i].orderPrice
        }
        return res;

    }

    $scope.groupBy= function (collection, property) {  // this function group the information by given property
        var i = 0, val, index,
            values = [], result = [];
        for (; i < collection.length; i++) {
            val = collection[i][property];
            val1 = collection[i]["isDiffSup"];
            val2 = collection[i]["diffSupStatus"];
            val3 = collection[i]["orderPrice"];
            val5 = collection[i]["defaultSup"];
            val4 = collection[i]["diffSup"];
            index = values.indexOf(val);
            if (index > -1)
            {
                result[index].push(collection[i]);
                result[index].orderNo=val;
                result[index].isDiffSup=val1;
                result[index].diffSupStatus=val2;
                result[index].orderPrice=val3;
                result[index].diffSup=val4;
                result[index].defaultSup=val5;
            }
            else {
                values.push(val);
                result.push([collection[i]]);
            }
        }
        return result;
    }
}]);