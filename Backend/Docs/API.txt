------------Employee APIs------------

1) get employee by id api
   /deushrm/api/employee/:id


------------Orders APIs--------------

1) get all orders
    /deushrm/api/orders

2) get order by id
    /deushrm/api/order/:id

------------Items APIs---------------

1) get all items
    /deusci/api/items

------------Lot APIs-----------------

1) get all lots
    /deusci/api/lots

2) get lot by id
    /deusci/api/lot/:id






