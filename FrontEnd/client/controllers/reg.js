var myApp = angular.module('myApp');
myApp.controller('RegController', ['$scope', '$http', '$location', '$routeParams', 'sharedProperties', function ($scope, $http, $location, $routeParams, sharedProperties) {
    $scope.baseURL = sharedProperties.getURL();
    console.log('RegController');
    $scope.isErrorAlert = false;
    $scope.isDoneAlert = false;

    //create new user object
    $scope.user = {
        name: "",
        address: "",
        email: "",
        site: "",
        role: "",
        supervisorId: "",
        issupervisor: "",
        password: ""
    }

    $scope.supervisors = [];

    //load init data 
    //loading data to the supervisor field
    $scope.loadData = function () {
        $http.get($scope.baseURL + '/deusci/api/employees/managers').then(successCallback, errorCallback);
        function successCallback(response) {
            $scope.supervisors = response.data;
            console.log(response);
        }
        function errorCallback(error) {
            console.log(error);
        }
    }

    //user registration function
    $scope.regUser = function () {

        //checking the password matching
        if ($scope.password != $scope.cnfPassword) {
            alert("Password in not matched!");
            $scope.isErrorAlert = true;

        }
        else {
            $scope.user.email = $scope.newEmail;
            $scope.user.name = $scope.fullname;
            $scope.user.address = $scope.address;
            $scope.user.role = $scope.userType;
            $scope.user.password = $scope.password;

            //setting user role and supervisor 
            if ($scope.supId.role == 2) {
                $scope.user.issupervisor = "false";
                $scope.user.supervisorId = null;
            } else {
                $scope.user.issupervisor = "true";
                $scope.user.supervisorId = $scope.supId.empId;
            }

            $scope.user.site = $scope.site;

            //send data to the server
            $http.post($scope.baseURL + '/deusci/api/employees', $scope.user).then(successCallback, errorCallback);
            function successCallback(response) {
                $scope.isDoneAlert = true;
                $location.path('/auth');
            }
            function errorCallback(error) {
                console.log(error);
            }
            console.log($scope.user);
        }
    }

    //redirect to the auth login page
    $scope.goSign = function () {
        $location.path('/auth');
    }
}]);