var myApp = angular.module('myApp');
myApp.controller('AuthController', ['$scope', '$http', '$location', '$routeParams', 'sharedProperties', function ($scope, $http, $location, $routeParams, sharedProperties) {
	console.log('AuthController');
	$scope.baseURL = sharedProperties.getURL();
	$scope.isErrorAlert = false;
	$scope.isDoneAlert = false;

	//make user object
	$scope.user = {
		name: "",
		email: "",
		role: "",
		supervisorId: "",
		empId: "",
		password :""


	}
//sign in function
	$scope.signin = function () {

		$http.get($scope.baseURL + '/deusci/api/employee/' + $scope.email).then(successCallback, errorCallback);
		function successCallback(response) {
			var result = response.data;
			//checking password against user input and result
			if ($scope.password != result.password) {
				console.log(result);
				$scope.isErrorAlert = true;
			} else {
				$scope.user.name = result.name;
				$scope.user.role = result.role;
				$scope.user.supervisorId = result.supervisorId;
				$scope.user.empId = result.empId;
				$scope.user.email = result.email;

				//store main infor
				localStorage['name'] = $scope.user.name;
				localStorage['email'] = $scope.user.email;
				localStorage['role'] = $scope.user.role;
				localStorage['defualtSup'] = $scope.user.supervisorId;
				localStorage['empId'] = $scope.user.empId;

				console.log($scope.user);
				$scope.isDoneAlert = true;
				window.location.reload(false);

				//redirect to the home
				$location.path('/home');

			}
		}
		function errorCallback(error) {
			console.log(error);
		}		
	}
	
	//redirect to the registration page
	$scope.goReg = function () {
		$location.path('/reg');
	}

}]);