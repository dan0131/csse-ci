var myApp = angular.module('myApp');
myApp.service('sharedProperties', function() {
    //var base_URL = 'http://deus-ci.azurewebsites.net';
    var base_URL = 'http://localhost:3000';
    var objectValue = {
        data: 'test object value'
    };
    
    return {
        getURL: function() {
            return base_URL;
        },
        getObject: function() {
            return objectValue;
        }
    }
});